// 引入依赖
const express = require("express")
const path = require("path")
const app = express();

// 监听4040端口
app.listen(4040, () => {
    console.log("服务器已启动，请访问： http://localhost:4040");
})

// 开放静态资源库
app.use(express.static(path.join(__dirname, "/public")));

data = [
    {
        name: "牙膏",
        unit_price: 10,
        num: 5,
    },
    {
        name: "水杯",
        unit_price: 23,
        num: 3,
    },
    {
        name: "沐浴露",
        unit_price: 35,
        num: 6,
    },
    {
        name: "洗发水",
        unit_price: 32,
        num: 6,
    },
    {
        name: "牙刷",
        unit_price: 5,
        num: 4,
    },
    {
        name: "洗衣液",
        unit_price: 59,
        num: 2,
    },
]

app.get("/get_data", (req, res) => {
    res.send(data);
})